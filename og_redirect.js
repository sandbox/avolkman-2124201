(function($){
  "use strict";

  Drupal.behaviors.og_redirect = {
    attach: function(context, settings) {
      var pathauto = $('#edit-og-redirect');
      pathauto.clone().appendTo('.form-item-source .field-prefix').after('/');
      pathauto.remove();
    }
  };

})(jQuery);
